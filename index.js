console.log("hello world");


/*
	if(statement){
		condition;
	}
*/

// The results of the expression 


numA = 1;

if(numA<0){
	console.log("hello from the recassigned value of numA")};
console.log(numA<0);

// It will not run because the expression now result to false.

let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York!")
}

//else if clause
/*
	-Executes a statement if previous conditions are false and if the specified condition is true.
	-The else if is optional and can be added to capture additional conditions to change the flow of a program.

*/

let numH = "1";

if(numH<0){
	console.log("Hello from (numH<0).");
}else if(numH>0){
	console.log("Hello from (numH>0).");
}
// We were able to run the else if() statement after we evaluated that the if condition was failed /false

// if the if() condiition was passed and run, we will no longer evaluate to else if() and end the process.

// else if is dependent with if, you cannot use else if clause alone.

if(numH!==1){
	console.log("Hello from numH === 1!");

}
else if(numH<0){
	console.log("hello from num>0");
}
else if(numH>0){
	conosle.log("Hello from the second else if clause!");
}

city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York!");
}else if(city === "Tokyo"){
	console.log("Welcome to Tokyo!");
}

// else statement

/*
	-Executes a statment if all other condotions are false / not met.
	-else if statemen is optional and can be added to capture any other result to cahnge the flow of program.
*/

numH = 2;

if(numH<0){
	console.log("Hello from if statement");
}
else if(numH>2){
	console.log("Hello from the first else if");
}
else if (numH>3){
	console.log("Hello from the second else if");
}
// since all of the conditions above are false / not met else statement will run.
else {
	console.log("hello from the else statement.")
}

// Since all of the preceeding if and else conditions were not met, the else statemnet was executed instead. 
// Else statement is also dependent with if statement, it cannot go alone.


 //if, else id and else statements with functions
 /*
	Most of the times we would like to use if, else if and if statement with functiuons to control the flow of our application.
 */ 

let message;

function determineTyphoonIntensity(windSpeed){
	if(windSpeed<0){
		return "Invalid argument!";
	}
	else if(windSpeed<=30){
		return "Not a typhoon yet!";
	}
	else if(windSpeed<=60){
		return "Tropical Depression detected!";
	}
	else if(windSpeed<=88){
		return "Tropical Storm detected!";
	}
	else if(windSpeed<=117){
		return "Severe Tropical Storm detected!";
	}
	else if(windSpeed>=118){
		return "Typhoon detected"
	}else {
		return "Invalid Argument"
	}
}

// Mini-activity 
console.log(determineTyphoonIntensity(85))

	message = determineTyphoonIntensity(119);
	if(message ==="Typhoon detected"){
		//console.warn() is a is a good way to print warning in our console that could help us developers act on certain output within our code
		console.warn(message);
	}

// [SECTION] Truthy or Falsy 
	// Javascript a "truthy" is a value taht is considered true when encountered in a boolean context.
	// Values are considered true unless defined otherwise.

	// Fallsy vaklues / exceptions for truthy
	/*
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined 
		7. NaN - not a number
	*/
if(null){
	console.log("Hello from null inside the if condition");
}
else {
	console.log("Hello form the else of the null condition");
}

// [SECTION] CONDITION OPERATOR / TERNARY OPERATOR
	/*
		The conditional operator
		1. Condition/expression
		2. Expression to execute if the condition is true or truthy.
		3. Expression if the condition is falsy 
		Syntax:
		(expression) ? ifTrue : ifFalse;
	*/

let ternaryResult = (1>18) ? true : false;
console.log(ternaryResult);

// [SECTION] SWITCH STATEMENT

	/*
		The Switch statement evaluates an expression and matches the expression's value to a case class.

	*/	

let day = prompt("What day of the week today?").toLowerCase();

switch(day){
	case 'monday':
		console.log('The color of the day is red!');
		// it means the code will stop here
		break;
	case 'tuesday':
		console.log('The color of the day is orange!');
		break;
	case 'wednesday':
		console.log('The color of the day is yellow!');
		break;
	case 'thursday':
		console.log('The color of the day is green!');
		break;
	case 'friday':
		console.log('The color of the day is blue!');
		break;
	case 'saturday':
		console.log('The color of the day is indigo!');
		break;
	case 'sunday':
		console.log('The color of the day is violet!');
		break;
	default:
		console.log('Please input a valid day!');
		break;
}

// [SECTION] TRY-CATCH FINALLY
		// try-catch statement for error handling
		// There are instances when the application returns an error/warning that is not necessarily an error in the contect of our code.
	// These errors are rresult of an attempt of the programming language to help developers in creating effecient code.

function ShowIntensity(windSpeed){
	try{
		// codes that will be executed or run
		alerat(determineTyphoonIntensity(windSpeed));
		alert("Intensity updates will show alert from try")
	}
	catch{
		console.warn(error.message);
	}
	finally{
		// continue execution of code regardless of success and failure of code execution in the try statement.
		alert("Intensity updates will show new alert from finally");
	}
}
ShowIntensity(119);